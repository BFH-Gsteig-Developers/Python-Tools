# Author: Philipp Weber

# Notes:
# boxes = 1 Square
# unit = Complete Row,Column,3x3
# peers = the other boxes in the same units as the inital box

#example unsolved puzzle (string):  ..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..
#example solved puzzle (string):    483921657967345821251876493548132976729564138136798245372689514814253769695417382

import time
COLS = '123456789'
ROWS = 'ABCDEFGHI'

def naked_twins(values):
    """Eliminate values using the naked twins strategy.
    Args:
        values(dict): a dictionary of the form {'box_name': '123456789', ...}

    Returns:
        the values dictionary with the naked twins eliminated from peers.
    """

    # Find all instances of naked twins
    naked_twins_found = []
    for unit in unitlist:
        for x in range(len(unit)):
            if len(values[unit[x]]) == 2:
                for y in range(x+1,len(unit)):
                    if values[unit[x]] == values[unit[y]]:
                        naked_twins_found.append([unit[x],unit[y]])
                        #Eliminate the naked twins as possibilities for their peers
                        digit = values[unit[x]]
                        for peer in peers[unit[x]]:
                            if peer in peers[unit[y]]:
                                values[peer] = values[peer].replace(digit[0],'')
                                values[peer] = values[peer].replace(digit[1],'')
    return values

def cross(a, b):
    """
    This function will return a list formed by all the possible concatentions of a letter s in string a with a letter t in string b 
    Args: 
        String of row,col names
    Returns: 
        List of all combinations row x col
    """
    return [s+t for s in a for t in b]

def grid_values(grid):
    """
    Convert grid into a dict of {square: char} with '123456789' for empties.
    Args:
        grid(string) - A grid in string form.
    Returns:
        A grid in dictionary form
            Keys: The boxes, e.g., 'A1'
            Values: The value in each box, e.g., '8'. If the box has no value, then the value will be '123456789'.
    """
    dic = {}
    if len(grid) == 81:
        for x in range(81):
            if grid[x] == '.':
                dic[boxes[x]] = '123456789'
            else:
                dic[boxes[x]] = grid[x]
    else:
        print('please insert a string with the correct length! (81 chars)')
    return dic

def display(values):
    """
    Display the values as a 2-D grid.
    Args:
        values(dict): The sudoku in dictionary form
    """
    width = 1+max(len(values[s]) for s in values)
    line = '+'.join(['-'*(width*3)]*3)
    for r in ROWS:
        print(''.join(values[r+c].center(width)+('|' if c in '36' else '')
                      for c in COLS))
        if r in 'CF': print(line)
    return


def eliminate(values):
    """
    Eliminate values from peers of each box with a single value.
    Args:
        values(dict): a dictionary of the form {'box_name': '123456789', ...}
    Returns:
        the values dictionary after eliminating solved boxes values from their peers.
    """
    solved_values = []
    for box in values:
        if len(values[box]) == 1:
            solved_values.append(box)
    for box in solved_values:
        digit = values[box]
        for peer in peers[box]:
            values[peer] = values[peer].replace(digit,'')
    return values

def only_choice(values):
    """
    Finalize all values that are the only choice for a unit.
    Args:
        values(dict): a dictionary of the form {'box_name': '123456789', ...}
    Returns:
        the values dictionary with after solving some boxes with their "only_choice".
    """
    for unit in unitlist:
        for digit in '123456789':
            dplaces = []
            for box in unit:
                if digit in values[box]:
                    dplaces.append(box)
            if len(dplaces) == 1:
                values[dplaces[0]] = digit
    return values

def reduce_puzzle(values):
    """
    This function tries to solve the puzzle with eliminate and only_choice as far as possible
    Args/Return:
        The sudoku in dictionary form
    """
    stalled = False
    while not stalled:
        # Check how many boxes have a determined value
        solved_values_before = len([box for box in values.keys() if len(values[box]) == 1])
        # Use the Eliminate Strategy
        values = eliminate(values)
        # Use the Only_choice Strategy
        values = only_choice(values)
        #Use the Naked Twins Strategy
        values = naked_twins(values)
        # Check how many boxes have a determined value, to compare
        solved_values_after = len([box for box in values.keys() if len(values[box]) == 1])
        # If no new values were added, stop the loop.
        stalled = solved_values_before == solved_values_after
        # Sanity check, return False if there is a box with zero available values:
        if len([box for box in values if len(values[box]) == 0]):
            return False
    return values

def search(values):
    """
    If the programm get stucked with eliminate and only_choice it uses death first search, creates a search tree and solves the sudoku
    Args/Return:
        The sudoku in dictionary form
    """
    # First, reduce the puzzle using the previous function
    values = reduce_puzzle(values)
    if values is False:
        return False ## Failed earlier
    if all(len(values[box]) == 1 for box in boxes): 
        return values ## Solved!
    # Choose one of the unfilled squares with the fewest possibilities
    chosen,box = min((len(values[box]), box) for box in boxes if len(values[box]) > 1)
    # Now use recursion to solve each one of the resulting sudokus, and if one returns a value (not False), return that answer!
    for value in values[box]:
        new_sudoku = values.copy()
        new_sudoku[box] = value
        attempt = search(new_sudoku)
        if attempt:
            return attempt

def solve(grid):
    """
    Find the solution to a Sudoku grid.
    Args:
        grid(string): a string representing a sudoku grid.
            Example: '2.............62....1....7...6..8...3...9...7...6..4...4....8....52.............3'
    Returns:
        The dictionary representation of the final sudoku grid. False if no solution exists.
    """
    #Create dictionary with the initial values ordered to boxes
    sudoku = grid_values(grid)
    #Solve puzzle
    sudoku = search(sudoku)
    return sudoku

#Create all single boxes
boxes = cross(ROWS,COLS)
#Create Diagonal Unit
#Insert in case of diagonal sudoku // diag_unit = [[x+y for x,y in zip(ROWS,COLS)],[x+y for x,y in zip(ROWS[::-1],COLS)]]
#Create a list with all units included
row_units = [cross(r, COLS) for r in ROWS]
column_units = [cross(ROWS, c) for c in COLS]
square_units = [cross(rs, cs) for rs in ('ABC','DEF','GHI') for cs in ('123','456','789')]
unitlist = row_units + column_units + square_units #Insert in case of diagonal sudoku // + diag_unit
#Create a dictionary with units ordered to boxes
units = dict((s, [u for u in unitlist if s in u]) for s in boxes)
#Create a dictionary which includes the peers of each specific box
peers = dict((s, set(sum(units[s],[]))-set([s])) for s in boxes)

if __name__ == '__main__':
    sudoku_grid = '...7.2.4.........7217....9.6.......3.2..48..........1..5..........3.......6......'
    #Measure performance
    start_time = time.clock()
    #Solve and display Sudoku
    display(solve(diag_sudoku_grid))
    #Record and print time
    end_time = time.clock()
    print("\n",round(end_time-start_time,3),"sec")
