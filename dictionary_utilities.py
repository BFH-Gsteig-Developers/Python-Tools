# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import ast
import traceback
import os
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@bfh.ch']
###############################################################################
###############################################################################
def read_text_to_dict(filename):
    """
    Reads a .txt file to a dict
    
    Parameters
    ----------
    filename : scalar, string
        Path to the .txt file
    
    Returns
    -------
    dict : scalar, dict
        Retreived dictionary from .txt file
    """
    s = open(filename, 'r').read()
    return ast.literal_eval(s)
    
def write_dict_to_text(dictionary,filename):
    """
    Writes dict to a .txt file
    
    Parameters
    ----------
    dictionnary : scalar, dictionary
        dictionary to write to the .txt file
        
    filename : scalar, string
        Path to the .txt file
    """
    if os.path.isfile(filename):
        os.remove(filename)
    target = open(filename, 'a')
    target.write(str(dictionary))
    target.close()
###############################################################################
###############################################################################             
if __name__ == "__main__":
    try:
        test_dict={"email":"sampleEmail@gmail.com",
                  "password":"samplePassword",
                  "server":"smtp.gmail.com",
                  "port":465}
        
        write_dict_to_text(dictionary=test_dict,filename='login.txt')
        
        login=read_text_to_dict('login.txt')
        
    except:
        traceback.print_exc()