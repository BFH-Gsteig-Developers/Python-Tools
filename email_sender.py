# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
from dictionary_utilities import read_text_to_dict
import time
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@bfh.ch']
###############################################################################
###############################################################################
class EmailSender():
    """
    Class to send emails
    """
    def __init__(self,login_file):
        """
        Constructor of EmailSender
        
        Parameters
        ----------
        login_file : scalar, string
            Path to the text file containing the login inforamtion
            (Please use DictionaryUtilities to create one)
        """
        self.__login_dict=read_text_to_dict(login_file)
        
        self.sender=self.__login_dict["email"]
        self.password=self.__login_dict["password"]
        self.server=self.__login_dict["server"]
        self.port=self.__login_dict["port"]
        
    def send_email(self,recipients,subject,text,attachements):
        """
        Sends email
        
        Paramters
        ---------
        recipients : list, string
            List of recipient email addresses
        subject : scalar, string
            Subject of the email
        text : scalar, string
            Plain text contained in the email
        attachements : list, string
            List of filepathes to be attached to the email
        """

        msg = MIMEMultipart()
        msg['From'] = self.sender
        msg['To'] = ", ".join(recipients)
        msg['Subject'] = subject
        body = text+"\n\n\nCurrent time is: {time}".format(time=time.strftime('%d.%m.%Y %H:%M:%S'))
    
        msg.attach(MIMEText(body))
    
        for attachement in attachements:
            print(attachement)
            with open(attachement, "rb") as fil:
                part = MIMEApplication(fil.read(), Name=basename(attachement))
                part["Content-Disposition"] = "attachment; filename={fileName}".format(fileName=basename(attachement))
                msg.attach(part)
        
        server = smtplib.SMTP_SSL(self.server, self.port)
        server.ehlo()
        server.login(self.sender, self.password)  
        server.sendmail(self.sender, recipients, msg.as_string())
        server.quit()
        server.close()
        print("Successfully sent the mail")    
###############################################################################
###############################################################################      
if __name__ == "__main__":
    mailer=EmailSender(login_file="C:/Users/lfb1/Desktop/login_ifms_email.txt")
    mailer.send_email(recipients=["lfb1@bfh.ch"],
                      subject="Face detected",
                      text="Hello User\nThere was a face detected, please check the attached image",
                      attachements=["C:/Users/lfb1/Pictures/PM1.PNG"])