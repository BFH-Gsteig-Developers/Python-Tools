# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import subprocess
import os
import traceback
###############################################################################
# Header
###############################################################################
__author__ = ["Benjamin Loeffel"]
__maintainer__ = ["Benjamin Loeffel"]
__email__ = ["benjamin.loeffel@fischerspindle.ch"]
###############################################################################
###############################################################################
class LatexBuilder():
    def __init__(self,latex_main_file,makeindex=False,biber=False):
        """
        Repository
        
        Parameters
        ----------
        latex_main_file : scalar, string
            path to the main latex file to compile
        makeindex : scalar, bool, optional
            flag for compiling makeindex, default = False
        biker : scalar, bool, optional
            flag for compiling biber, default = False        
        """
        self.latex_main_file=latex_main_file
        self.makeindex=makeindex
        self.biber=biber
        
    def build(self):
        """
        Builds the specified latex files
        Implements the standard latex toolchain (pdflatex, makeindex, biker, pdflatex)
        """
        print("Building Latex")
        file_name=os.path.basename(self.latex_main_file).split('.')[0]
        cwd=os.path.dirname(self.latex_main_file)   
        
        compile_command='xelatex -synctex=1 -interaction=nonstopmode --shell-escape "{file_name}.tex"'.format(file_name=file_name)
        self.execute_command(command=compile_command,cwd=cwd,verbose=False)
        
        if self.makeindex==True:
            command='makeindex "{file_name}.idx"'.format(file_name=file_name)
            self.execute_command(command=command,cwd=cwd,verbose=False)
            
        if self.biber==True:
            command='biber "{file_name}.bcf"'.format(file_name=file_name)
            self.execute_command(command=command,cwd=cwd,verbose=True)
     
        self.execute_command(command=compile_command,cwd=cwd,verbose=False)
        print("Build successfull")

    def execute_command(self,command,cwd=None,verbose=False):
        """
        Executes a bash command
        
        Parameters
        ----------
        command : scalar, string
            bash command to execute
            
        cwd : scalar, string, optional
            Wrking directory for the specified bash command, default = None (Python cwd)
            
        Returns
        -------
        stdoutput,stderroutput
        """
        PIPE = subprocess.PIPE
        process = subprocess.Popen(command.split(' '), cwd=cwd, stdout=PIPE, stderr=PIPE)
        stdoutput, stderroutput = process.communicate()
        
            
        if verbose==True:
            print("")
            print(command)
            print('')
            print(stdoutput)
            print('')
            print(stderroutput)
            print('')
            
        return stdoutput, stderroutput
        
if __name__=="__main__":
    try:
        latex_builder=LatexBuilder(latex_main_file="C://Users//bloeffel//Documents//Git//Fischer-Spindle//latex//presentation//fischer.tex")
        latex_builder.build()
    except:
        traceback.print_exc()