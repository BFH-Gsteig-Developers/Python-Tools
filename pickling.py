# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import pickle
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__maintainer__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@fischerspindle.ch']
###############################################################################
###############################################################################       
def load_pickle_data(file_name):
    """
    Loads pickled objects
    
    Parameters
    ----------
    file_name : scalar, string
        path to the file name to load
        
    Returns
    -------
    data : objects
        objects contained in file
    """
    with open(file_name,"rb") as f:
        return pickle.load(f)
    
def save_pickle_data(objects,file_name):
    """
    Saves objects as pickled data
    
    Parameters
    ----------
    objects : list, objects
        objects to save
    file_name : scalar, string
        path to the file name to save
    """
    with open(file_name, "wb") as f:
        pickle.dump(objects, f)