# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import subprocess
import os
import datetime
import shutil
import traceback
from EmailSender import EmailSender
###############################################################################
# Header
###############################################################################
__author__ = ["Benjamin Loeffel"]
__maintainer__ = ["Benjamin Loeffel"]
__email__ = ["benjamin.loeffel@gmx.ch"]
###############################################################################
###############################################################################
class Repository():
    GIT_ROOT="/home/ifms/Desktop/Git"
    MOUNT_ROOT="/home/ifms/Desktop/Mount/TI/fbm/Projects/Team IFMS/32_Admin AG Fuerst/AUTO_DEPLOY"
    IGNORE_PATTERNS = ('CVS','.git','tmp','.svn') 
    
    def __init__(self,path):
        """
        Repository
        
        Parameters
        ----------
        path : scalar, string
            relative path to the local repository in git root
        """
        self.git_path=os.path.join(self.GIT_ROOT,path)
        self.mount_path=os.path.join(self.MOUNT_ROOT,path)
        
        self.latex_present=False
        
        self.transfered=False
        
    def set_latex_files(self,latex_files):
        """
        Sets the latex files to build, optional
        
        Parameters
        ----------
        latex_files : list, string
            list of latex files to build
        """
        if len(latex_files)>0:
            self.latex_files=[]
            for dirpath, dirnames, filenames in os.walk(self.git_path):
                for filename in filenames:
                    for latex_file in latex_files:
                        if filename==latex_file:
                            self.latex_files.append(os.path.join(dirpath, filename))               
            
            if len(latex_files)!=len(self.latex_files):
                raise ValueError("Not all latex files could have been found")
            
            self.latex_present=True   
        
    def deploy(self):
        """
        Performs all deploy operations
        """
        try:
            self.log_file=open(os.path.join(self.mount_path,"Auto_Deploy_Log.txt"),"w")        
            self.log_message(message="Deploy started")  
            self.pull()
#            self.build_latex_files()
#            self.cleanDirectory()
#            self.transfer()
            self.log_message(message="Deploy finished")
        except:
            traceback.print_exc()
        finally:
            self.log_file.close()
            
    def pull(self,):
        """
        Pulls the repository
        """
        self.log_message(message="Pull started") 
        os.chdir(self.git_path)
        command="git pull origin master"
        self.pull_output, self.pull_error=self.execute_command(command=command)
        
        command="git log -1"
        self.git_log_output,_=self.execute_command(command=command)
        self.log_message(message="Pull finished") 
        
        
    def build_latex_files(self):
        """
        Builds the specified latex files
        Implements the standard latex toolchain (pdflatex, makeindex, biker, pdflatex)
        """
        raise NotADirectoryError("use latex_compiler")
                        
            
    def clean_directory(self):
        """
        Cleans the auto deploy directory
        """
        print("Cleaning directory")
        shutil.rmtree(self.mountPath, ignore_errors=True)
              
    def transfer(self):
        """
        Transfers the local repository to P:
        """
        print("Transfering")
        
        command="rsync -av '{source}' '{destination}' --exclude '.git'".format(source=self.gitPath, destination=self.mountRoot)
        subprocess.call([command],shell=True)
        self.transfered=True
        
    def execute_command(self,command,cwd=None,verbose=False):
        """
        Executes a bash command
        
        Parameters
        ----------
        command : scalar, string
            bash command to execute
            
        cwd : scalar, string, optional
            Wrking directory for the specified bash command, default = None (Python cwd)
            
        Returns
        -------
        stdoutput,stderroutput
        """
        PIPE = subprocess.PIPE
        if cwd==None:
            process = subprocess.Popen(command.split(' '), cwd=cwd, stdout=PIPE, stderr=PIPE)
        else:
            process = subprocess.Popen(command.split(' '), stdout=PIPE, stderr=PIPE)
        stdoutput, stderroutput = process.communicate()
        
        if "error" in stderroutput:
            raise IOError()
            
        if verbose==True:
            print("")
            print(command)
            print('')
            print(stdoutput)
            print('')
            print(stderroutput)
            print('')
        return stdoutput, stderroutput
        
    def write_log(self):
        """
        Writes the log to indicate status of deployment
        """
        print("Writing Log")
        if os.path.exists(self.mountPath)==False:
            os.mkdir(self.mountPath)
            
        
        self.__Separator(logfile=self.logFile)
        print("Auto Deploy Log",file=self.logFile)   
        self.__Separator(logfile=self.logFile)
        print("Time : {time}".format(time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")),file=self.logFile)
        self.__Separator(logfile=self.logFile)
        print("Pull Output",file=self.logFile)
        print("")
        print("{stdoutput}".format(stdoutput=self.stdoutput),file=self.logFile)
        print("{stderroutput}".format(stderroutput=self.stderroutput),file=self.logFile)
        self.__Separator(logfile=self.logFile)
        print("Last commit",file=self.logFile)
        print("")
        print("{commitstdoutput}".format(commitstdoutput=self.commitstdoutput),file=self.logFile)
        self.__Separator(logfile=self.logFile)
        print("Transfered : {transfered}".format(transfered=self.transfered),file=self.logFile)
        self.logFile.close()
        self.transfered=False
        
    def error_log(self,error):
        """
        Writes the Errorlog if there is a Error
        
        Paramters
        ---------
        error : scalar, string
            traceback of the exception as a string
        """
        print("Writing ErrorLog")
        for directory in ["",self.mountRoot]:
            ErrorLog=open(os.path.join(directory,"ErrorLog.txt"),"a")
            self.__Separator(logfile=ErrorLog)
            print("ErrorLog of Auto Deploy",file=ErrorLog)   
            self.__Separator(logfile=ErrorLog)
            print("Time : {time}".format(time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")),file=ErrorLog)
            self.__Separator(logfile=ErrorLog)
            print("Error is: {Error}".format(Error=error),file=ErrorLog)
            ErrorLog.close()
            
        
    def log_message(self,message,filename=None):
        """
        Logs a message
        
        Parameters
        ----------
        message : scalar, string
            message to process
        filename : file, optional
            file to write or 'None' if output on console, default = 'None'
        """
        for output in [None, self.log_file]:
            print("",file=output)
            self._seperator(filename=output)
            print("Event logged",file=output)
            self._seperator(filename=output)
            print("Time : {time}".format(time=datetime.datetime.now().isoformat()),file=output)
            print("Meassage : {message}".format(message=message),file=output)
            self._seperator(filename=output)
            print("",file=output)

    def _seperator(self,filename):
        """
        Prints a seperator
        """
        nChars=40
        print("#"*nChars,file=filename)
        
###############################################################################
###############################################################################
if __name__ == "__main__":
    
    Lehre_Multiple_Choice=Repository(path="Lehre-Multiple-Choice")
    
    Lehre_Lift=Repository(path="Lehre-Lift")
    
    Lehre_Schwingungsmessung_Getriebewelle=Repository(path="Lehre-Schwingungsmessung-Getriebewelle")   
    
    Pilotanlage_Batteriefertigung=Repository(path="Pilotanlage-Batteriefertigung")
    
    SBB_On_Board_Monitoring=Repository(path="SBB-On-Board-Monitoring")
    SBB_On_Board_Monitoring.set_latex_files(latex_files=["On-Board-Monitoring.tex"])
    
    Lehre_Schwingungsdemonstrator=Repository(path="Lehre-Schwingungsdemonstrator")
    
    McMonitoring_Waelzlagerpruefstand=Repository(path="McMonitoring-Waelzlagerpruefstand")
    
    Mathis_Trocknungsanlage=Repository(path="Mathis-Trocknungsanlage")
    
    Georg_Fischer_Bodenanalyse=Repository(path="Georg-Fischer-Bodenanalyse")
    
    repoList=[]
    repoList.append(Lehre_Multiple_Choice)
    repoList.append(Lehre_Lift)
    repoList.append(Lehre_Schwingungsmessung_Getriebewelle)
    repoList.append(Pilotanlage_Batteriefertigung)
    repoList.append(SBB_On_Board_Monitoring)
    repoList.append(Lehre_Schwingungsdemonstrator)
    repoList.append(McMonitoring_Waelzlagerpruefstand)
    repoList.append(Mathis_Trocknungsanlage)
    repoList.append(Georg_Fischer_Bodenanalyse)
    
    
    SBB_On_Board_Monitoring.deploy()
    
#    for repo in repoList:
#        try:
#            repo.deploy()
#                   
#        except:
#            error = traceback.format_exc()
#            repo.ErrorLog(error)
#            Mailer=EmailSender(loginFile="ifms_email_login.txt",recipients=["jrm1@bfh.ch","lfb1@bfh.ch"])
#            Mailer.SendEmail(subject="AutoDeploy Error occured",
#                     text="Hello Marco\nThere was an error during AutoDeploy, please verify the log\n\nkind regards\n\nauto deploy server",attachements=["ErrorLog.txt"])
