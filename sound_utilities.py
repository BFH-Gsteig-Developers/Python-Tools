# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import numpy as np
import matplotlib.pyplot as plt
import pyaudio
import wave
import os
import time
from scipy.io.wavfile import read
###############################################################################
# Header
###############################################################################
__author__ = 'Benjamin Loeffel'
__maintainer__ = 'Benjamin Loeffel'
__email__ = 'benjamin.loeffel@gmx.ch'
###############################################################################
# Methods
###############################################################################
def read_wav(wav_file,time_stamps=False):
    """
    Reads the specified wav file and returns a numpy array
    
    Parameters
    ----------
    wav_file : scalar, string
        path to wav file
    time_stamps : scalar, bool, optional
        flag to return timestamps, default = False
        
    Returns
    -------
    f_sampling,signal : tuple
        sampling frequency / Hz, signal as np.array with size corresponding to channel count
    f_sampling,signal,time_stamps : tuple
        sampling frequency / Hz, signal as np.array with size corresponding to channel count, time_stamps / s, 1D array
    """
    f_sampling,signal=read(wav_file)
    if time_stamps==False:
        return f_sampling,signal
    else:
        return f_sampling,signal,np.arange(0,len(signal),1)*(1/f_sampling)
###############################################################################
# Classes
###############################################################################
class SoundRecorder():
    CHUNK = 1000
    FORMAT = pyaudio.paFloat32
    CHANNELS = 1
    def __init__(self,name,duration,f_sampling=96000,data_path=os.getcwd()):
        """
        SoundRecorder class
        
        Parameters
        ----------
        name : string
            Name of the measurement session, default = timestamp     
        duration : float
            Duration of measurement / seconds
        f_sampling : int, optional
            sampling frequency / hz, default = 96000
        data_path : string, optional
            Directory to archive .wav files, default = script directory
        """
        self.data_path=data_path
        self.name=name
        self.duration=duration
        self.f_sampling=f_sampling
        
    def record(self,time_stamp=False):
        """
        Starts measurement session
        
        Parameters
        ----------
        time_stamp : scalar, bool, optional
            Flag to prevent overwriting existing data by adding a time_stamp, default = False
        
        Returns
        -------
        t : numpy array float,
            Timestaps / seconds
        values : numpy array float,
            Measured values / 1
        """
        if time_stamp==True:
            time_stamp=time.strftime(" %Y_%m_%d_%H_%M_%S")
            self.name=self.name+time_stamp
            
        
        p = pyaudio.PyAudio()
        
        stream = p.open(format=self.FORMAT,
                        channels=self.CHANNELS,
                        rate=self.f_sampling,
                        input=True,
                        frames_per_buffer=self.CHUNK)
        
        print("start recording")
        
        frames = []
        
        for i in range(0, int(self.f_sampling / self.CHUNK * self.duration)):
            data = stream.read(self.CHUNK)
            frames.append(data)
        
        print("stop recording")
        
        stream.stop_stream()
        stream.close()
        p.terminate()
        
        wf = wave.open(self.name+".wav","wb")
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(p.get_sample_size(self.FORMAT))
        wf.setframerate(self.f_sampling)
        wf.writeframes(b''.join(frames))
        wf.close()         
###############################################################################
###############################################################################   
if __name__=="__main__":
    sound_recorder=SoundRecorder(name="test_record",duration=1,f_sampling=44000)
    sound_recorder.record()
    
    f_sampling,signal,time_stamps=read_wav("test_record.wav",time_stamps=True)
      
    plt.figure()
    plt.plot(time_stamps,signal)
    plt.title("Sound data")
    plt.xlabel("Time / s")
    plt.ylabel("Amplitude / 1")
    plt.grid("on")