# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import cProfile, pstats, io
import datetime
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@bfh.ch']
###############################################################################
###############################################################################
class PerformanceMeter():
    PROFILING_LOG="Profiling_Log.txt"
    def __init__(self,name,log_file_name=PROFILING_LOG):
        """
        Constructor of PerformanceMeter
        
        Parameters
        ----------
        name : scalar, string
            name of the PerformanceMeter
        log_file_name : scalar, string , optional
            path to the log file
        """
        self.name=name
        self.log_file_name=log_file_name
        self.pr = cProfile.Profile()
        self.start()
        
    def start(self):
        """
        Starts the profiler
        """
        self.start_time=datetime.datetime.now()
        self.pr.enable()
        
    def stop(self):
        """
        Stops the profiler
        """
        self.pr.disable()
        self.stop_time=datetime.datetime.now()
        self.elapsed_time=(self.stop_time-self.start_time).total_seconds()

    def write_log(self):
        """
        Writes the profiling log
        """
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(self.pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        run_stats=s.getvalue()
        
        self.log_file=open(self.log_file_name,"w")
        print("",file=self.log_file)
        seperator(log_file=self.log_file,nChars=250)
        seperator(log_file=self.log_file,nChars=250)
        print("Section Profiled",file=self.log_file)
        seperator(log_file=self.log_file,nChars=250)
        seperator(log_file=self.log_file,nChars=250)
        print("name : {name}".format(name=self.name),file=self.log_file)
        print("start : {start_time}".format(start_time=self.start_time.isoformat()),file=self.log_file)
        print("stop : {stop_time}".format(stop_time=self.stop_time.isoformat()),file=self.log_file)
        print("elapsed time /s : {elapsed_time}".format(elapsed_time=self.elapsed_time),file=self.log_file)
        seperator(log_file=self.log_file,nChars=250)
        print("",file=self.log_file)
        print(run_stats,file=self.log_file)
        print("",file=self.log_file)
        seperator(log_file=self.log_file,nChars=250)
        self.log_file.close()
        
def seperator(log_file,nChars=60):
    """
    Prints a seperator
    """
    print("#"*nChars,file=log_file)
        
def start_message():
    """
    Prints a start message
    """
    start_time=datetime.datetime.now()
    print("")
    seperator(log_file=None)
    print("Start")
    print("Starttime : {start_time}".format(start_time=start_time.isoformat()))
    seperator(log_file=None)
    return start_time
    
def stop_message(start_time=None):
    """
    Prints a stop message
    """
    stop_time=datetime.datetime.now()
    print("")
    seperator(log_file=None)
    print("Stop")
    print("Stoptime : {stop_time}".format(stop_time=stop_time.isoformat()))
    if type(start_time)==datetime.datetime:
        elapsed_time=(stop_time-start_time).total_seconds()
        print("Elapsed time {elapsed_time} s".format(elapsed_time=elapsed_time))
    seperator(log_file=None)